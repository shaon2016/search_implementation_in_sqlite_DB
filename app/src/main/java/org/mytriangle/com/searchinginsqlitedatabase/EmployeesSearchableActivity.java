package org.mytriangle.com.searchinginsqlitedatabase;

import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Shaon on 8/11/2016.
 */
public class EmployeesSearchableActivity extends AppCompatActivity {

    TextView textName, textAge;
    SqliteHandler myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employess_searchable_activity);

        textName = (TextView) findViewById(R.id.textName);
        textAge = (TextView) findViewById(R.id.textAge);

        int id = getIntent().getExtras().getInt("id");
        myDatabase = new SqliteHandler(EmployeesSearchableActivity.this);

        showEmployeesDescription(id);

    }

    private void showEmployeesDescription(int id) {

        String age = "";
        String name = "";

        Cursor cursor = myDatabase.getAllEmployeesDescription(id + 1);
        if (cursor.getCount() < 1) {
            age += "No Data Available";
            name += "No Data Available";

        } else {
            cursor.moveToFirst();
            do {
                age = cursor.getString(cursor.getColumnIndex(myDatabase.EMPLOYEE_AGE));
                name = cursor.getString(cursor.getColumnIndex(myDatabase.EMPLOYEE_NAME));
            }
            while (cursor.moveToNext());
        }
        // set Name
        textName.setText(name);
        //set Age
        textAge.setText(age);
    }
}
