package org.mytriangle.com.searchinginsqlitedatabase;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Shaon on 8/11/2016.
 */
public class SqliteHandler extends SQLiteOpenHelper {
    private static final String TAG = SqliteHandler.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "man";
    private static final String TABLE_NAME = "employees";
    private static final String EMPLOYEE_ID = "id";
    public static final String EMPLOYEE_NAME = "name";
    public static final String EMPLOYEE_AGE = "age";


    public SqliteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String table_sql = "CREATE TABLE employees (id INTEGER PRIMARY KEY" +
                ", name TEXT, age INTEGER)";

        db.execSQL(table_sql);

        Log.d(TAG, "Database tables created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    void addEmployess(String name, String age) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(EMPLOYEE_NAME, name);
        contentValues.put(EMPLOYEE_AGE, age);

        long id = db.insert(TABLE_NAME, null, contentValues);
        db.close(); // Closing database connection

        Log.d(TAG, "New employess inserted into sqlite: " + id);
    }

    public ArrayList<String> getAllName() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> employeeNames = new ArrayList<String>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();


            do {
                String name = cursor.getString(cursor.getColumnIndex(EMPLOYEE_NAME));

                employeeNames.add(name);

            } while (cursor.moveToNext());

        }

        cursor.close();
        db.close();

        return employeeNames;

    }

    public Cursor getAllEmployeesDescription(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] selections = {String.valueOf(id)};
        String columns[] = {EMPLOYEE_NAME, EMPLOYEE_AGE};
        Cursor cursor = db.query(TABLE_NAME, columns, EMPLOYEE_ID + "=?",
                selections, null, null, null);
        //db.close();
        return cursor;
    }
}
